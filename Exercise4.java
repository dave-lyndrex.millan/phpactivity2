/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaExercises;

/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise4 {
    
    4. Why should we opt for isEmpty() over size?
            
      -The top reasons for using isempty rather than size would be: it is more expressive 
       (the code is easier to read and to maintain) it is faster, in some cases by orders of magnitude. 
        Another issue on why we use isEmpty() over size is Time execution.
        In a short String, you won't notice any execution time issue, but when working with long Strings the 
        difference (in seconds) is considerable. isEmpty() expected shows constant values, but
        count increases considerably the time of execution when the String gets longer. Time always matter.
    
}
