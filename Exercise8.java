/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaExercises;
import java.util.*;
/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise8 {
    
    public static void countMapKeys(Map<String, String> sports) {
       int count = 0;
       for (Map.Entry<String, String> value : sports.entrySet()) {
           if (value.getKey().substring(0, 9).contains("concordia")) {
               count++;
           }
       }
       System.out.println(count);
   }
    
   public static void main(String[] args) {
        Map<String, String> sports= new HashMap<>();
        sports.put("acconcordia", "Basketball");
        sports.put("concordia1", "Volleyball");
        sports.put("concordia", "Table Tennis");
        sports.put("concordia2", "Succer");
        sports.put("condensada", "Badminton");
        countMapKeys(sports);
        
   }
   
  }

