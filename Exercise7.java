/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaExercises;

import java.util.*;

/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise7 {

   public class myFavFruits {

        private String myFavFruits;
      

        public myFavFruits(String myFavFruits) {
            this. myFavFruits= myFavFruits;
        }

        public String getMyFavFruits() {
            return myFavFruits;
        }

        public void setMyFavFruits(String myFavFruits) {
            this.myFavFruits = myFavFruits;
        }
    }
     
      public class justineFavFruits {

        
        private String justineFavFruits;

        public justineFavFruits(String justineFavFruits) {
            this. justineFavFruits= justineFavFruits;
        }

        public String getJustineFavFruits() {
            return justineFavFruits;
        }

        public void setJustineFavFruits(String justineFavFruits) {
            this.justineFavFruits = justineFavFruits;
        }
    }

    public static void main(String args[]) {
        ArrayList<String> myFavFruits = new ArrayList<String>();
        myFavFruits.add("Apple");
        myFavFruits.add("Orange");
        myFavFruits.add("Grapes");
        myFavFruits.add("Soursop");
        myFavFruits.add("Mango");
        
        ArrayList<String> justineFavFruits = new ArrayList<String>();
        justineFavFruits.add("Jackfruit");
        justineFavFruits.add("Banana");
        justineFavFruits.add("Grapes");
        justineFavFruits.add("Caimito");
        justineFavFruits.add("Mango");
        justineFavFruits.add("Apple");
        justineFavFruits.add("Orange");
        
        System.out.println("First set content: " + myFavFruits);
        System.out.println("Second Set content: " + justineFavFruits);
        justineFavFruits.removeAll(myFavFruits);
        System.out.print("HashSet content: ");
        System.out.println(justineFavFruits);

    }
}
