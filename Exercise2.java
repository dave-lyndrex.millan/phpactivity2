/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaExercises;

import java.util.*;

/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise2 {

    static void randomize(int arr[], int n){
        Random rand = new Random();
        for (int i = n-1; i > 0; i--) {
            int j = rand.nextInt(i);
             int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        
        }
        System.out.print("After being randomized:" );
        System.out.println(Arrays.toString(arr));
    
    }
    
    public static void main(String args[]) {
         int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
         System.out.println("Original Format: " + (Arrays.toString(arr)));
         int n = arr.length;
         randomize (arr, n);
    }
}
