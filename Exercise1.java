/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaExercises;

import java.util.*;

/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise1 {

    public class PN_Student {

        private String studentname;

        public PN_Student(String studentname) {
            this.studentname = studentname;
        }

        public String getStudentname() {
            return studentname;
        }

        public void setStudentname(String studentname) {
            this.studentname = studentname;
        }
    }

    public static void main(String args[]) {
        ArrayList<String> arraylist = new ArrayList<String>();
        arraylist.add("Dave Lyndrex Millan");
        arraylist.add("Guian Victor Amancio");
        arraylist.add("Sernel Asunto");
        arraylist.add("John Dave Pacinio");
        arraylist.add("Melchor Casipong");
        arraylist.add("Kerwien Bengil");
        arraylist.add("Patrick Carl Glinogo");
        arraylist.add("Justine Ambrad");
        arraylist.add("Daryll Vildosola");
        arraylist.add("Rehnan Ramil");
        arraylist.add("Juan Enciso");
        
        Collections.sort(arraylist);
        
        System.out.println("PN JAVA BOYS:");
        System.out.println(arraylist);

    }

}
