/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaExercises;

import java.util.*;

/**
 *
 * @author 2ndyrGroupA
 */
public class Exercise3 {

    public static void MinToFront(ArrayList<Integer> arrayOfNumbers) {
        int inputLength = arrayOfNumbers.size();
        int min = 0;
        for (int ctr = 0; ctr < inputLength; ctr++) {
            if (arrayOfNumbers.get(ctr) < arrayOfNumbers.get(min)) {
                min = ctr;
            }
        }
        System.out.println("Arraylist: " + arrayOfNumbers);
        arrayOfNumbers.add(0, arrayOfNumbers.remove(min)); 
        System.out.println("Get the minimum value: " + arrayOfNumbers);
        System.out.println("");
    }

    public static void main(String args[]) {
        ArrayList<Integer> arrayOfNumbers = new ArrayList<>();

        arrayOfNumbers.add(5);
        arrayOfNumbers.add(15);
        arrayOfNumbers.add(6);
        arrayOfNumbers.add(2);
        arrayOfNumbers.add(8);
        arrayOfNumbers.add(25);
        arrayOfNumbers.add(1);
        arrayOfNumbers.add(7);
        arrayOfNumbers.add(4);

        MinToFront(arrayOfNumbers);
    }
}
